public class Mobil implements Global, Detail {
    private String name, color;
    private boolean horn;

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    @Override
    public boolean hasHorn() {
        return horn;
    }

    public void setHorn(boolean horn) {
        this.horn = horn;
    }
}
