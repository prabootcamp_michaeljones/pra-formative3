public class Main {
    public static void main(String[] args) {
        Mobil bmx = new Mobil();
        bmx.setColor("Black");
        bmx.setName("TDR 3000");
        bmx.setHorn(true);

        System.out.println("Object Mobil");
        System.out.println("Name = " + bmx.getName());
        System.out.println("Color = " + bmx.getColor());
        System.out.println("Ada Lampu? = " + bmx.hasHorn());

        Kipas polytron = new Kipas();
        polytron.canPutar(true);
        polytron.setColor("Blue");
        polytron.setName("Polytron Kece");
        polytron.setSpeed(3);

        System.out.println("\nObject Kipas");
        System.out.println("Name = " + polytron.getName());
        System.out.println("Color = " + polytron.getColor());
        System.out.println("Kecepatan = " + polytron.getSpeed());
        System.out.println("Bisa Mutar? = " + polytron.isSwing());
    }
}
