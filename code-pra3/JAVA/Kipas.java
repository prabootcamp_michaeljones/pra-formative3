public class Kipas implements Putar, Global {
    private int speed;
    private String name, color;
    private boolean putar;

    @Override
    public void canPutar(boolean putar) {
        this.putar = putar;
    }

    public boolean isSwing() {
        return putar;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }
}